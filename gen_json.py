# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""SFTP application description generator."""

import json
import sys
import os

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

UPSTREAM_NOTEBOOKS = [
    ("sftp", REPOSITORY + "/sftp:" + VERSION)
]

ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'core_limit': {
        'value': 0.2,
        'description': 'SFTP core limit'
    },
    'memory_limit': {
        'value': 512 * (1024**2),
        'description': 'SFTP memory limit (bytes)'
    }
}

def sftp_service(memory_limit, core_limit, image):
    """
    :rtype: dict
    """
    service = {
        'name': "sftp-server",
        'image': image,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [
            {
                'name': 'SFTP access',
                'protocol': 'tcp',
                'port_number': 22,
                'url_template': 'sftp://{ip_port}/'
            }
        ],
        'environment': [
            ['HOME', '/mnt/workspace'],
            ['SSH_PUBLIC_KEY', '']
        ],
        'volumes': [],
        'command': None,
        'work_dir': '/mnt/workspace',
        'total_count': 1,
        'essential_count': 1,
        'replicas': 1,
        'startup_order': 0
    }

    return service


if __name__ == '__main__':
    for app_name, image in UPSTREAM_NOTEBOOKS:
        app = {
            'name': app_name,
            'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
            'will_end': False,
            'size': 512,
            'services': [
                sftp_service(options["memory_limit"]["value"], options["core_limit"]["value"], image)
            ]
        }

        json.dump(app, open(app_name + ".json", "w"), sort_keys=True, indent=4)

    print("ZApps written")

