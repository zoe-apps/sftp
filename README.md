# OpenSSH-based SFTP workspace access

With this application you will be able to connect to your workspace via SFTP.

On windows, for example, you can use the [WinSCP](https://winscp.net/eng/download.php) client.

To connect, use your Zoe username and paste your SSH public key below.

